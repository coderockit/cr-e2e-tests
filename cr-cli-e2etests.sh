#!/bin/sh
export PATH="../cr-cli/build/0.0.1+4/darwin21:$PATH"
export LIBREALM_DART_PATH="$PWD/../cr-cli/binary/macos"
export LIBCODEROCKIT_PATH="$PWD/../cr-cli/binary/macos"

# 1) Make sure that the cr api server is running and upto date with the code -- compile and restart the cr api server if needed

# 2) Make sure that the cr command line tool is upto date with the code -- compile and install the cr command line tool if needed

# 3) Run the cr command line tool e2e tests
#cleanup from previous runs
cr version -v
cr scanfs -f cr-test-project,../cr-cli/test -v
cr scanfs -v -l

cr payload -v
cr payload -v -l
mv ../cr-cli/test/src/xml/test5.xml ../cr-cli/test/src/xml/orig.test5.xml
cr scanfs -v -u
mv ../cr-cli/test/src/xml/orig.test5.xml ../cr-cli/test/src/xml/test5.xml
cr payload -v -l
mv ../cr-cli/test/src/xml/test2.xml ../cr-cli/test/src/xml/orig.test2.xml
cr scanfs -v -u
mv ../cr-cli/test/src/xml/orig.test2.xml ../cr-cli/test/src/xml/test2.xml
cr payload -v -l
cr scanfs -f cr-test-project,../cr-cli/test -v

cr login
cr push
# cr plugin 

cr import -v

